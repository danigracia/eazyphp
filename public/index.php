<?php

require_once __DIR__ . '/../includes/app.php';

use Controllers\AppController;
use MVC\Router;

$router = new Router();

//Routes
$router->get("/", [AppController::class, "index"]);

// Checks if the routes exists and asigns the functions to the controller
$router->comprobarRutas();
