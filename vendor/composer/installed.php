<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'microframework',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'db2da860c768c6546577f0a479ef988e1d8bd967',
        'name' => 'danig/eazyphp',
        'dev' => true,
    ),
    'versions' => array(
        'danig/eazyphp' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'microframework',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'db2da860c768c6546577f0a479ef988e1d8bd967',
            'dev_requirement' => false,
        ),
        'phpmailer/phpmailer' => array(
            'pretty_version' => 'v6.5.3',
            'version' => '6.5.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpmailer/phpmailer',
            'aliases' => array(),
            'reference' => 'baeb7cde6b60b1286912690ab0693c7789a31e71',
            'dev_requirement' => false,
        ),
    ),
);
