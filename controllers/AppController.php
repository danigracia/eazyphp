<?php

namespace Controllers;

use MVC\Router;
use Model\Example;

class AppController
{
    public static function index(Router $router)
    {
        $router->render("index", [
            "name" => "Eazy PHP",
        ]);
    }
}
