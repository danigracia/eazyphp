<?php

namespace Model;

class Example extends EazyORM
{
    //Base de datos
    protected static $table = "example";
    protected static $columnsDB = ["id", "name"];

    //Errores
    public static $alerts = [];

    public $id;
    public $name;


    public function __construct($args = [])
    {
        $this->id = $args["id"] ?? null;
        $this->name = $args["name"] ?? "";
    }
}
